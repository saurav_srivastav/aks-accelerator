## 05 Testing Access to the Cluster


### 5.1 Installing Kubectl
> If you have `kubectl` installed already, skip this step.

If you do not already have `Kubectl`, the Kubernetes CLI, the Azure CLI provides a simple way to install it:

```
➜  az aks install-cli
```

### 5.2 Configuring Kubectl

The Azure CLI also provides a simple mechanism for configuring `~/.kube/config` (the `kubectl` config file) with credentials for the newly created cluster:

```
➜  az aks get-credentials  \
--name aks-accelerator-aks  \
--resource-group aks-accelerator-rg
```

This command will retrieve, using Azure CLI credentials, the necessary secrets from Kubernetes and merge them into the local config map (alongside the details for any existing clusters):

```
apiVersion: v1
clusters:
- cluster:
    certificate-authority-data: <base64 encoded cert>
    server: https://aks-accelerator-<id>.hcp.westeurope.azmk8s.io:443
  name: aks-accelerator-aks
contexts:
- context:
    cluster: aks-accelerator-aks
    user: clusterUser_aks-accelerator-rg_aks-accelerator-aks
  name: aks-accelerator-aks
current-context: aks-accelerator-aks
kind: Config
preferences: {}
users:
- name: clusterUser_aks-accelerator-rg_aks-accelerator-aks
  user:
    client-certificate-data: <base64 encoded cert>
    client-key-data: <base64 encoded key>
    token: <token>
```  

At this point, kubectl's context is set correctly. If you need to check it:

```
➜  kubectl config current-context

aks-accelerator-aks
```
 

### 5.3 Connecting to the Cluster

We can now connect to the cluster:

```
➜  kubectl cluster-info

Kubernetes master is running at https://aks-accelerator-xxxxxxxx.hcp.westeurope.azmk8s.io:443
addon-http-application-routing-default-http-backend is running at https://aks-accelerator-xxxxxxxx.hcp.westeurope.azmk8s.io:443/api/v1/namespaces/kube-system/services/addon-http-application-routing-default-http-backend/proxy
addon-http-application-routing-nginx-ingress is running at http://xx.xx.xx.xx:80 http://xx.xx.xx.xx:443
Heapster is running at https://aks-accelerator-xxxxxxxx.hcp.westeurope.azmk8s.io:443/api/v1/namespaces/kube-system/services/heapster/proxy
CoreDNS is running at https://aks-accelerator-xxxxxxxx.hcp.westeurope.azmk8s.io:443/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy
kubernetes-dashboard is running at https://aks-accelerator-xxxxxxxx.hcp.westeurope.azmk8s.io:443/api/v1/namespaces/kube-system/services/kubernetes-dashboard/proxy
Metrics-server is running at https://aks-accelerator-xxxxxxxx.hcp.westeurope.azmk8s.io:443/api/v1/namespaces/kube-system/services/https:metrics-server:/proxy

```

```
➜  kubectl get nodes

NAME                      STATUS   ROLES   AGE   VERSION
aks-kscclrtr-40561189-0   Ready    agent   43m   v1.12.6
```

> To see what else can be retrieved with `get`, see `kubectl api-resources`.

## 5.4 Potential Issues

Kubectl authenticates with the Kubernetes API server with TLS Mutual (Client Certificate) authentication.

In Enterprise environments, corporate internet proxies sometimes perform SSL interception (SSLi).

If you are not able to authenticate with the API server, check whether SSL interception is in place. If it is, then you may need to ask your network security team for an exception.


## 5.5 Scaling the Cluster

### 5.5.1 Using Terraform
To increase or decrease the number of nodes in the cluster, we can simply change the `agent pool` count:

```
resource "azurerm_kubernetes_cluster" "aks_cluster" {

  "agent_pool_profile" {
    ...
    count   = "3"
    ...
    }
```

### 5.5.2 Using Azure CLI
Using Azure CLI, we can use `az aks scale`:

```
➜  az aks scale \
--name aks-accelerator-aks  \
--resource-group aks-accelerator-rg \
--node-count 3
```



## Next
In the next section, we will configure Gitlab to deploy to AKS.

< [04 I - Creating an AKS Cluster Using Terraform](04_I_creating_aks_via_terraform.md) | 05 Testing Access to the Cluster | [06 Connecting Gitlab to AKS](06_connecting_gitlab_to_aks.md)