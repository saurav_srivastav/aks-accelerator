output "aks_cluster_id" {
  value = "${azurerm_kubernetes_cluster.aks_cluster.id}"
}

output "aks_resource_group_name" {
  value = "${azurerm_resource_group.aks_rg.name}"
}

output "aks_log_analytics_workspace_id" {
  value = "${azurerm_log_analytics_workspace.aks_law.id}"
}